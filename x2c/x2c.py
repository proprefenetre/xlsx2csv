#! /usr/bin/env python

import csv
from pathlib import Path
import sys
from typing import Sequence

import click
import openpyxl as pyxl
from openpyxl.utils.exceptions import InvalidFileException


def write_csv(fp: Path, data: Sequence[Sequence[str]]) -> None:
    """Write data to csv file at FP."""
    with fp.open("w", newline="") as csvfile:
        writer = csv.writer(csvfile)
        for row in data:
            writer.writerow(row)


@click.command()
@click.argument(
    "inp", nargs=-1, type=click.Path(exists=True, readable=True, path_type=Path)
)
@click.option(
    "-s",
    "--sheet",
    type=str,
    default="0",
    help="Select worksheets. Defaults to the first sheet. Pass the name of one or more sheets, or 'all'."
)
def run(inp: Path, sheet: str) -> None:
    """Convert .xlsx files to csv."""
    inp = inp[0]

    try:
        workbook = pyxl.load_workbook(filename=inp, data_only=True)
        if sheet == "0":
            write_csv(fn.with_suffix(".csv"), workbook.active.values)
        else:
            if sheet == "all":
                sheets = workbook.sheetnames
            else:
                sheets = sheet.split()

            for sheet in sheets:
                write_csv(inp.parent / f"{inp.stem}_{sheet}.csv", workbook[sheet].values)
    except InvalidFileException:
        print(f"Invalid file: {str(inp)}")
        sys.exit(1)

    sys.exit(0)


if __name__ == "__main__":
    run()
